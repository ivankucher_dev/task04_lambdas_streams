package com.epam.trainings.controller;

import com.epam.trainings.model.command.CarExecutor;
import com.epam.trainings.model.command.Command;
import com.epam.trainings.model.command.ConcreteCommand.TurnLeftCommand;
import com.epam.trainings.model.command.Receiver.Car;
import com.epam.trainings.model.command.Receiver.CarMoves;
import com.epam.trainings.model.lambdas.Returner;
import com.epam.trainings.model.streamapi.StreamWorker;
import com.epam.trainings.model.testanalyzerapp.TextAnalyzerApp;
import com.epam.trainings.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ViewController {

  private static final int LIST_OF_UNIQUE_WORDS = 1;
  private static final int NUM_OF_UNIQUE_WORDS = 2;
  private static final int OC_NUM_OF_EACH_SYMBOLS = 3;
  private static final int WORD_AND_FREQUENCY = 4;
  private View view;
  private Car car;
  private TextAnalyzerApp textAnalyzerApp;
  private static Logger log = LogManager.getLogger(ViewController.class.getName());

  public ViewController(View view) {
    this.view = view;
  }

  public String firstTask(int num1, int num2, int num3) {
    Returner maxNumber = (a, b, c) -> Stream.of(a, b, c).max(Integer::compareTo).get().intValue();
    Returner averageNum = (a, b, c) -> (a + b + c) / 3;
    StringBuilder stb = new StringBuilder();
    stb.append("Max number = " + maxNumber.nums(num1, num2, num3));
    stb.append(", average = " + averageNum.nums(num1, num2, num3));
    return stb.toString();
  }

  public String commandTask(String commandName, String arg) {
    CarExecutor carExecutor = new CarExecutor();
    car = new Car(arg);
    String execute = null;
    if (commandName.equals(car.getCommandsName().get(0))) {
      execute = carExecutor.executeOperation(car::stop);
    }
    if (commandName.equals(car.getCommandsName().get(2))) {
      execute =
          carExecutor.executeOperation(
              new Command() {
                public String execute() {
                  return car.turnRight();
                }
              });

      if (commandName.equals(car.getCommandsName().get(3))) {
        execute = carExecutor.executeOperation(new TurnLeftCommand(car));
      }
    }
    if (commandName.equals(car.getCommandsName().get(1))) {
      execute = carExecutor.executeOperation(() -> "Accelerate car " + arg);
    }

    return execute;
  }

  public String streamTask(int amount, int lowLimit, int highLimit) {
    StreamWorker streamWorker = new StreamWorker(amount, lowLimit, highLimit);
    return streamWorker.getStatisticInfo();
  }

  public String textAnalyzerTask(List<String> text, int command) {
    textAnalyzerApp = new TextAnalyzerApp(text);
    if (command == LIST_OF_UNIQUE_WORDS) {
      return listToString(textAnalyzerApp.listOfUniqueWords());
    } else if (command == NUM_OF_UNIQUE_WORDS) {
      return String.valueOf(textAnalyzerApp.numOfUniqueWords());
    } else if (command == OC_NUM_OF_EACH_SYMBOLS) {
      return listToString(textAnalyzerApp.occurenceNumbersOfEachSymbol());
    } else if (command == WORD_AND_FREQUENCY) {
      return listToString(textAnalyzerApp.mapOfWordAndCount());
    } else {
      log.warn("Command not found! returning null");
      return null;
    }
  }

  public List<String> getTxtAppFunctionsList() {
    return textAnalyzerApp.getFunctionsForUserList();
  }

  private String listToString(List<String> list) {
    return list.stream()
            .map(Objects::toString)
            .collect(Collectors.joining("\n"));
  }

  public List<String> getCommandsName() {
    List<String> commandsName = new ArrayList<>();
    try {
      Class classobj = CarMoves.class;
      Method[] methods = classobj.getMethods();
      for (Method method : methods) {
        commandsName.add(method.getName());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return commandsName;
  }
}
