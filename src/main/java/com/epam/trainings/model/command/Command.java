package com.epam.trainings.model.command;

public interface Command {
  String execute();
}
