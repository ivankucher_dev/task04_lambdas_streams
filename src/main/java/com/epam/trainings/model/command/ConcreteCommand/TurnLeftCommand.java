package com.epam.trainings.model.command.ConcreteCommand;

import com.epam.trainings.model.command.Command;
import com.epam.trainings.model.command.Receiver.Car;

public class TurnLeftCommand implements Command {
  private final Car car;

  public TurnLeftCommand(Car car) {
    super();
    this.car = car;
  }

  @Override
  public String execute() {
    return car.turnLeft();
  }
}
