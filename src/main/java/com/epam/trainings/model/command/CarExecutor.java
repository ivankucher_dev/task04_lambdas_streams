package com.epam.trainings.model.command;

import java.util.ArrayList;
import java.util.List;

public class CarExecutor {
  private final List<Command> carOperations = new ArrayList<>();

  public String executeOperation(Command command) {
    carOperations.add(command);
    return command.execute();
  }
}
