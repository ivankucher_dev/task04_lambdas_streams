package com.epam.trainings.model.command.Receiver;

import com.epam.trainings.model.testanalyzerapp.TextAnalyzerApp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Car implements CarMoves {
  private String arg;
  private static Logger log = LogManager.getLogger(TextAnalyzerApp.class.getName());
  private List<String> commandsName;

  public Car(String arg) {
    this.arg = arg;
    commandsName = new ArrayList<>();
    setCommandsName();
  }

  private void setCommandsName(){
    commandsName.add("stop");
    commandsName.add("accelerate");
    commandsName.add("turn right");
    commandsName.add("turn left");
  }

  public List<String> getCommandsName() {
    return commandsName;
  }

  @Override
  public String stop() {
    log.info("Function stop car called");
    String command = arg + " stopped successfully";
    return command;
  }

  @Override
  public String accelerate() {
    log.info("Function accelerate car called");
    String command = arg + " accelerated successfully";
    return command;
  }

  @Override
  public String turnRight() {
    log.info("Function turn right car called");
    String command = arg + " turned right successfully";
    return command;
  }

  @Override
  public String turnLeft() {
    log.info("Function turn left car called");
    String command = arg + " turned left successfully";
    return command;
  }
}
