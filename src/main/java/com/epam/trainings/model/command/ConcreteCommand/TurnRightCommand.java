package com.epam.trainings.model.command.ConcreteCommand;

import com.epam.trainings.model.command.Command;
import com.epam.trainings.model.command.Receiver.Car;

public class TurnRightCommand implements Command {
  private final Car car;

  public TurnRightCommand(Car car) {
    super();
    this.car = car;
  }

  @Override
  public String execute() {
    return car.turnRight();
  }
}
