package com.epam.trainings.model.command.ConcreteCommand;

import com.epam.trainings.model.command.Command;
import com.epam.trainings.model.command.Receiver.Car;

public class AccelerateCommand implements Command {

  private final Car car;

  public AccelerateCommand(Car car) {
    super();
    this.car = car;
  }

  @Override
  public String execute() {
    return car.accelerate();
  }
}
