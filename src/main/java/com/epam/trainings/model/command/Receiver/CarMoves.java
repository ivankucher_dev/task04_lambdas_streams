package com.epam.trainings.model.command.Receiver;

public interface CarMoves {

  String stop();

  String accelerate();

  String turnRight();

  String turnLeft();
}
