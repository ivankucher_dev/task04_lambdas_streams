package com.epam.trainings.model.lambdas;

@FunctionalInterface
public interface Returner {
  int nums(int a, int b, int c);
}
