package com.epam.trainings.model.testanalyzerapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static com.epam.trainings.Const.*;

public class TextAnalyzerApp {
    private List<String> text;
    private String wordsRegex = "[\\p{Punct}\\s]+";
    private static Logger log = LogManager.getLogger(TextAnalyzerApp.class.getName());

   public TextAnalyzerApp(List<String> text){
       this.text = text;
    }

    public long numOfUniqueWords(){
       log.info("Num of unique words, preparing...");
        text = getWordList(text);
        long uniqueWordCount =text.stream()
                .map(String::toLowerCase)
                // Build a map from word -> frequency
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                // stream the frequency map entries
                .entrySet().stream()
                // filter to retain unique words (with frequency == 1)
                .filter(e -> e.getValue() == 1)
                // count them
                .count();
        return uniqueWordCount;
    }

    public List<String> listOfUniqueWords(){
        log.info("List of unique words, preparing...");
        text = getWordList(text);
        List<String> uniqueWordList =text.stream()
                .map(String::toLowerCase)
                // Build a map from (word || count)
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                // stream the frequency map entries
                .entrySet().stream()
                // filter to unique words (with count == 1)
                .filter(e -> e.getValue() == 1).map(Map.Entry::getKey).collect(Collectors.toList());
        uniqueWordList.sort(Comparator.naturalOrder());
        return uniqueWordList;
    }

    public List<String> mapOfWordAndCount(){
        log.info("Map of words and their frequencies, preparing...");
        text = getWordList(text);
        List<String> uniqueWordsList =text.stream()
                .map(String::toLowerCase)
                // Build a map from word -> frequency
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet()
                .stream()
                .map(e->StringFormat.format(e.getKey(),e.getValue()))
                .collect(Collectors.toList());

        return uniqueWordsList;
    }

    public List<String> occurenceNumbersOfEachSymbol(){
        log.info("Occurence numbers of each symbols, preparing...");
    List<String> list =
        text.stream().map(c->c.split(""))
            .flatMap(Arrays::stream)
                .filter(e->!Character.isUpperCase(e.charAt(0)))
            .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .entrySet()
                .stream()
                .map(e->StringFormat.formatMapCharFrequency(e.getKey(),e.getValue()))
                .collect(Collectors.toList());
        return list;
    }

    public static List<String> getFunctionsForUserList(){
        List<String> functions = new ArrayList<>();
        functions.add(TXT_ANLZ_FUNC_1);
        functions.add(TXT_ANLZ_FUNC_2);
        functions.add(TXT_ANLZ_FUNC_3);
        functions.add(TXT_ANLZ_FUNC_4);
       return functions;
    }

    private List<String> getWordList(List<String>... lists) {
       log.info("Gettings words list");
        return Stream.of(lists)
                .flatMap(Collection::stream)
                .flatMap(str -> Arrays.stream(str.split(wordsRegex)))
                .collect(Collectors.toList());
    }
}
