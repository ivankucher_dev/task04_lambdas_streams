package com.epam.trainings.model.testanalyzerapp;

public class StringFormat {
  public static String format(String key, long value) {
    String output = String.format("[word = %s] || [frequency = %d]", key, value);
    return output;
  }

  public static String formatMapCharFrequency(String key, long value) {
    String output = String.format("[symbol = %s] || [frequency = %d]", key, value);
    return output;
  }
}
