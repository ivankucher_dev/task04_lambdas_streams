package com.epam.trainings.model.streamapi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamWorker {

  private List<Integer> list;
  private int amountOfElements;
  private int low;
  private int high;
  private StringBuilder statistic;
    private static Logger log = LogManager.getLogger(StreamWorker.class.getName());

  public StreamWorker(int amountOfElements, int low, int high) {
    list = new ArrayList<>();
    this.amountOfElements = amountOfElements;
    this.low = low;
    this.high = high;
    statistic = new StringBuilder();
  }

  public List getRandomList() {
      IntStream stream = new Random().ints(amountOfElements, low, high);
    list = stream.boxed().collect(Collectors.toList());
    return list;
  }

  public String getStatisticInfo(){
      if(list.isEmpty()){
          log.info("List is empty! Generating new list of random numbers");
          getRandomList();
      }
      double avarage = list.stream()
              .mapToDouble(a->a)
              .average()
              .getAsDouble();

      int max = list.stream()
              .max(Integer::compareTo)
              .get();

      int sum  = list.stream()
              .reduce((a,b)->a+b)
              .get();

      sum  = list.stream()
              .reduce(0,Integer::sum);

      List<Integer> upperAvg =list
              .stream()
              .filter(num->num>avarage)
              .collect(Collectors.toList());
      statisticToString(list,avarage,max,sum,upperAvg);
      return statistic.toString();
  }

  private void statisticToString(List<Integer> initialList,double avg,
                                 int max , int sum,List<Integer> upperAvg){

      statistic.append("Auto generated list : "+ list
              .stream()
              .map(Object::toString)
              .collect(Collectors.joining(",")));
      statistic.append("\nmax value = "+max);
      statistic.append("\nsum = "+sum);
      statistic.append("\naverage value = "+avg);
    if (!upperAvg.isEmpty()) {
      statistic.append(
          "\nList of nums greater then avg : "
              + upperAvg
                  .stream()
                  .map(Object::toString)
                  .collect(Collectors.joining(",")));
      }else{
        statistic.append("\nThere is no nums greater then avg");
    }
  }
}
