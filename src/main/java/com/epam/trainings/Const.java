package com.epam.trainings;

public class Const {
  public static final String TXT_ANLZ_FUNC_1 = "1) list of unique words ";
  public static final String TXT_ANLZ_FUNC_2 = "2) num of unique words";
  public static final String TXT_ANLZ_FUNC_3 =
      "3) occurrence number of each symbols except upper case ";
  public static final String TXT_ANLZ_FUNC_4 = "4) map of words and their frequencies ";
}
