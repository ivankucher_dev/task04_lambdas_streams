package com.epam.trainings.view;

import com.epam.trainings.controller.ViewController;
import com.epam.trainings.model.lambdas.Returner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

public class View {
  private static final int FIRST_TASK_MENU = 1;
  private static final int COMMAND_MENU = 2;
  private static final int STREAM_MENU = 3;
  private static final int TEXT_ANYLZER_APP_MENU = 4;
  private static final int END = 5;
  private static Logger log = LogManager.getLogger(View.class.getName());
  private Scanner scan;
  private ViewController controller;

  public View() {
    controller = new ViewController(this);
    scan = new Scanner(System.in);
    initView();
  }

  public ViewController getController() {
    return controller;
  }

  public void initView() {

    int choice;
    do {
      log.info(
          "Init view : \n1)Three number task\n2)Commands menu\n3)Streams menu\n4)Text analyzer menu");
      choice = scan.nextInt();
      switch (choice) {
        case FIRST_TASK_MENU:
          callFirstTaskMenu();
          break;
        case COMMAND_MENU:
          callCommandMenu();
          break;

        case STREAM_MENU:
          callStreamCommand();
          break;

        case TEXT_ANYLZER_APP_MENU:
          callTextAnalyzerMenu();
          break;
      }
    } while (choice != END);
  }

  private void callFirstTaskMenu() {
    log.info("Three number task menu\n Enter three numbers: ");
    int choise[] = new int[3];
    int i = 0;
    do {
      choise[i] = scan.nextInt();
      i++;
    } while (i != 3);
    String result = controller.firstTask(choise[0], choise[1], choise[2]);
    System.out.println(result);
  }

  private void callCommandMenu() {
    String command;
    String carName;
    int count = 0;
    do {
      scan.nextLine();
      log.info("Welcome to command menu\nAvaliable commands : \n");
      controller.getCommandsName().forEach(e -> System.out.println(e));
      log.info("Enter command : ");
      command = scan.nextLine();
      for (String e : controller.getCommandsName()) {
        if (e.trim().equals(command)) {
          count++;
        }
      }
      log.info("\nEnter car name : ");
      carName = scan.nextLine();
    } while (count == 0);
    System.out.println(controller.commandTask(command, carName));
  }

  private void callStreamCommand() {
    int amount;
    int lowLimit;
    int highLimit;
    log.info("Welcome to stream menu\nEnter amount :");
    amount = scan.nextInt();
    log.info("Enter low limit number for random : ");
    lowLimit = scan.nextInt();
    log.info("Enter high limit number for random : ");
    highLimit = scan.nextInt();
    System.out.println(controller.streamTask(amount, lowLimit, highLimit));
  }

  private void callTextAnalyzerMenu() {
    List<String> text = new ArrayList<>();
    String txt = null;
    int input = 0;
    do {
      scan.nextLine();
      log.info("\nEnter text to analyze : ");
      txt = scan.nextLine();
      if (!txt.equals("")) text.add(txt);
    } while (!txt.equals(""));
    do {
        log.info("To exit , input 5 in command");
      log.info("Avaliable commands for analize : ");
      controller.getTxtAppFunctionsList().forEach(e -> System.out.println(e));
      log.info("Enter : ");
      input = scan.nextInt();
      System.out.println(controller.textAnalyzerTask(text, input));
    } while (input != 5);
  }
}
